#!/usr/bin/env bash
# PURPOSE: run commands on all repos available in current dir & subdirs

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptBaseName=$(basename $scriptName)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables

# -- auto: common helper functions
printTitle()  { echo;echo; msg="${*}"; linesno=$(( ${#msg} + 4 )); msglines=$(for i in `seq ${linesno}`;do echo -n "=";done);echo -e "${msglines}\n# ${msg} #\n${msglines}"; }
gitReposList() { find . -type d -name .git -exec dirname {} \; -follow; }
# -- /auto: common helper functions


# -- args parsing
# reset params
params=""
# no params
[[ -z "${1}" ]] && NOARGS=true
# other params
while [[ -n "${1}" ]]
do
  case "${1}" in
    -f|--force)
      FORCE=true
    ;;
    -h|--help)
      HELP=true
    ;;
    -s|--silent)
      SILENT=true
    ;;
    -v|--verbose)
      VERBOSE=true
    ;;
    -w|--workdir)
      shift
      WORKDIR=${1}
    ;;
    -l|--list-urls)
      params="git remote -v|grep 'fetch'|awk '{print \$2}'"
      SILENT=true
    ;;
    -u|--update-all)
      params='git fetch -p && git checkout master && git pull;echo "--";'
    ;;
    *)
      params+="${1} "
    ;;
  esac
  shift
done


# script usage
if [[ -z "${params}" ]] || [[ "true" = ${HELP} ]] || [[ "true" = ${NOARGS} ]];then
  printTitle "USAGE"
  echo "[${scriptBaseName}] ${scriptName} [OPTIONS] <COMMAND> [ARGS]"
  printTitle "OPTIONS"
  echo "-f|--force        force update of repos list (do not use cache)"
  echo "-h|--help         print out this help"
  echo "-s|--silent       enable silent mode"
  echo "-v|--verbose      enable verbose output"
  echo "-w|--workdir PATH use PATH as WORKDIR (default:${PWD})"
  printTitle "PREDEFINED COMMANDS"
  echo "-l|--list-urls     print list of remote-urls"
  echo "-u|--update-all    fetch changes, switch back to master branch & pull"
  echo
  exit 0
fi

# clear screen first
clear
# set workdir (current path by default)
[[ -n "${WORKDIR}" ]] || WORKDIR="${PWD}"

# get available git repos
if [[ "true" = ${FORCE} ]] || [[ ! -f "${WORKDIR}/.repos.list" ]]
then
  [[ "true" = ${SILENT} ]] || echo "[${scriptBaseName}] fetching list of repositories"
  REPOS_LIST="$(gitReposList)"
  echo "${REPOS_LIST}" >${WORKDIR}/.repos.list
else
  [[ "true" = ${SILENT} ]] || echo "[${scriptBaseName}] reading list of repositories from cache"
  REPOS_LIST="$(cat ${WORKDIR}/.repos.list)"
fi

# get skipped git repos (egrep pattern)
if [[ -z "${REPOS_SKIP_PATTERN}" ]] && [[ -f "${WORKDIR}/.repos.skip" ]]
then
  REPOS_SKIP_PATTERN=$(cat ${WORKDIR}/.repos.skip | tr -d "\n")
fi

# set effective list of repos to work with
repos=""
for repo in ${REPOS_LIST}
do
  if [[ -n "${REPOS_SKIP_PATTERN}" ]] && grep -qE "${REPOS_SKIP_PATTERN}" <<< "${repo}"
  then
    [[ "true" = ${VERBOSE} ]] && echo "[${scriptName}] (-) repo: ${repo}"
  else
    [[ "true" = ${VERBOSE} ]] && echo "[${scriptName}] (+) repo: ${repo}"
    repos+="${repo} "
  fi
done

# switch to workdir
pushd ${WORKDIR} >/dev/null

# run command(s) on all repos
commands="${params}"
if [[ "true" = ${VERBOSE} ]]
then
  echo -e "\033[32m"
  printTitle "SUMMARY"
  echo "REPOS_LIST: ${REPOS_LIST}" | tr "\n" ":";echo
  echo "REPOS_SKIP_PATTERN: ${REPOS_SKIP_PATTERN}"
  echo "repos: ${repos}"
  echo "commands: ${commands}"
  echo -e "\033[0m"
fi

for repo in ${repos}
do
  if [[ "true" = ${VERBOSE} ]] || [[ "true" != ${SILENT} ]];then
    printTitle "${repo}"
  fi
  cd ${repo} >/dev/null || false
  eval ${commands}
  cd - >/dev/null
done
popd >/dev/null 2>&1
[[ "true" = ${SILENT} ]] || ( echo;echo; )

# eof
